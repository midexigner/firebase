// Initialize Firebase
  var config = {
    apiKey: "AIzaSyC6BMe9ZNly2Mc8rAL0tGE3c6ZeNj8tGpg",
    authDomain: "project-384779291768.firebaseapp.com",
    databaseURL: "https://project-384779291768.firebaseio.com",
    projectId: "project-384779291768",
    storageBucket: "project-384779291768.appspot.com",
    messagingSenderId: "431524349944"
  };

  firebase.initializeApp(config);
  // make auth and firestore references
  const auth = firebase.auth();
  const db = firebase.firestore();
  db.settings({ timestampsInSnapshots: true }); 