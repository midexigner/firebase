const cafeList = document.querySelector('#cafe-list');
const form = document.querySelector('#add-cafe-form');
// create element and render cafe
function renderCafe(doc){
	let li = document.createElement('li');
	let name = document.createElement('span');
	let city = document.createElement('span');
	let cross = document.createElement('div');

	li.setAttribute('data-id',doc.id);
	name.textContent = doc.data().name;
	city.textContent = doc.data().city;
	cross.textContent = 'x';

	li.appendChild(name);
	li.appendChild(city);
	li.appendChild(cross);

	cafeList.appendChild(li);

	// deleting data
	cross.addEventListener('click',(e)=>{
		e.stopPropagation();
		let id = e.target.parentElement.getAttribute('data-id');
		db.collection('cafes').doc(id).delete();
	});
}

// getting data
// db.collection('cafes').get()
// db.collection('cafes').where('city','==','USA').get() 
// db.collection('cafes').where('city','>','n').get() 
// db.collection('cafes').where('city','<','n').get() 
// db.collection('cafes').orderBy('name').get()
// db.collection('cafes').where('city','==','USA').orderBy('name').get()
/*db.collection('cafes').get().then((snapshot)=> {
	snapshot.docs.forEach(doc =>{
		//console.log(doc.data());
		renderCafe(doc);
	})
//console.log(snapshot.docs);
})*/


// saving data
form.addEventListener('submit',(e)=>{
	e.preventDefault();
	db.collection('cafes').add({
		name:form.name.value,
		city:form.city.value,
	});
	name:form.name.value = '';
	city:form.city.value = '';
})

//real-time listener
db.collection('cafes').orderBy('city').onSnapshot(snapshot =>{
let changes = snapshot.docChanges();
console.log(changes);
changes.forEach(change =>{
		console.log(change.doc.data());
		if(change.type == 'added'){
		renderCafe(change.doc);
		}else if(change.type == 'removed'){
		let li = cafeList.querySelector('[data-id='+change.doc.id+']');
		cafeList.removeChild(li);
		}
	})
})

// updating records
/*db.collection('cafes').doc('pvRWwJrYEDVSq9qQlpsv').update({
    name: 'mario ',
    city: 'San Jose'
});*/

// setting data
/*db.collection('cafes').doc('pvRWwJrYEDVSq9qQlpsv').set({
    city: 'California'
});*/