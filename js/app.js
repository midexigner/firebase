// DOM elements
const guideList = document.querySelector('.guides');
const loggedOutLinks = document.querySelectorAll('.logged-out');
const loggedInLinks = document.querySelectorAll('.logged-in');
const setupUI = (user) => {
  if (user) {
    // toggle user UI elements
    loggedInLinks.forEach(item => item.style.display = 'block');
    loggedOutLinks.forEach(item => item.style.display = 'none');
  } else {
    // toggle user elements
    loggedInLinks.forEach(item => item.style.display = 'none');
    loggedOutLinks.forEach(item => item.style.display = 'block');
  }
};
// setup guides
const setupGuides = (data) => {
  if (data.length) {
  let html = '';
  data.forEach(doc => {
    const guide = doc.data();
    const li = `
      <li class="collection-item">
        <div class="collapsible-header grey-text"> ${guide.title} </div>
        <div class="collapsible-body"> ${guide.content} </div>
      </li>
    `;
    html += li;
  });
  guideList.innerHTML = html
  }else{
    guideList.innerHTML = '<h5 class="center-align">Login to view guides</h5>';
  }
};

// setup materialize components
window.addEventListener('DOMContentLoaded', () => {

    var modals = document.querySelectorAll('.modal');
    M.Modal.init(modals);
  
    var items = document.querySelectorAll('.collapsible');
    M.Collapsible.init(items);

    var elems = document.querySelectorAll('.tooltipped');
    M.Tooltip.init(elems);
  
  });